package mmu
import chisel3._
import chisel3.stage.ChiselStage
import chisel3.util._

class CSRMMU extends Bundle {
  val satpPPN = UInt(44.W)
  val asid = UInt(16.W)
  val sum = Bool()
  val enableSv39 = Bool()
  val enableLSVM = Bool()
  val mxr = Bool()
  val tvm = Bool()
  val tw = Bool()
  val tsr = Bool()
}

// Naive Bus Master to slave
class NaiveBusM2S extends Bundle {
  val memRreq   = Output(Bool())
  val memAddr   = Output(UInt(64.W))
  val memRdata  = Input(UInt(64.W))
  val memRvalid = Input(Bool())
  val memWdata  = Output(UInt(64.W))
  val memWmask  = Output(UInt(8.W))
  val memWen    = Output(Bool())
  val memSize   = Output(UInt(3.W))
  val memWrDone = Input(Bool())
}

// MEM2MMU Request
class MEM2MMU extends Bundle {
  val reqVAddr      = Output(UInt(64.W))
  val reqReady      = Output(Bool())
  val respPAddr     = Input(UInt(64.W))
  val respValid     = Input(Bool())
  val respPageFault = Input(Bool())
}

class MMUIO extends Bundle {
  val mem2mmu = Flipped(new MEM2MMU)
  val flush = Input(Bool())
  val dmemreq = new NaiveBusM2S
  val csr2mmu = Input(new CSRMMU)
}

class NaiveBusAdapter extends Module {
  val io = IO(new Bundle {
    val data_req = Output(Bool())
    val data_wr = Output(Bool())
    val data_size = Output(UInt(2.W))
    val data_addr = Output(UInt(64.W))
    val data_read_valid = Input(Bool())
    val data_rdata = Input(UInt(64.W))
    val membus = Flipped(new NaiveBusM2S)
  })
  io.data_req := io.membus.memRreq
  io.data_wr := false.B
  io.data_size := 8.U
  io.data_addr := io.membus.memAddr
  io.membus.memRdata := io.data_rdata
  io.membus.memRvalid := io.data_read_valid
  io.membus.memWrDone := false.B
}

// LSU send VAddr to MMU, MMU returns the PAddr with valid signal
// LSU then use the translated address to access the memory
// When LSU is accessing memory, the bus is busy, and the IF is paused
// When IF is busy, and a load/store reached the execution stage, ?
// PTW need to offer the addr handshake and the data handshake interface
// IFU send PC to MMU, MMU returns the PAddr
// LSU AddrTranslate -> Send Load -> Get response / Exception
// If LSU is busy, inst req is blocked

class MMU (isDMMU: Boolean) extends Module {
  val io = IO(new MMUIO)
  val tlb = Module(new TLB)
  val ptw = Module(new PTW(isDMMU))
  // PTW <> MMU
  ptw.io.reqReady          := io.mem2mmu.reqReady
  ptw.io.reqVAddr          := io.mem2mmu.reqVAddr
  io.mem2mmu.respPAddr     := ptw.io.respPaddr
  io.mem2mmu.respValid     := ptw.io.respValid
  io.mem2mmu.respPageFault := ptw.io.pageFault
  
  // TLB
  ptw.io.tlbQuery <> tlb.io.tlbQuery
  ptw.io.tlbUpdate <> tlb.io.tlbUpdate
  ptw.io.flush := io.flush
  tlb.io.flush := io.flush
  // CSR ----> PTW Signals
  ptw.io.enableSv39        := io.csr2mmu.enableSv39
  ptw.io.translation_ls_en := io.csr2mmu.enableLSVM
  ptw.io.satp_PPN          := io.csr2mmu.satpPPN
  ptw.io.mxr               := io.csr2mmu.mxr
  ptw.io.satp_ASID         := io.csr2mmu.asid
  
  // ptw ctrl
  ptw.io.flush             := io.flush
  ptw.io.reqIsStore        := false.B
  io.dmemreq <> ptw.io.memReq
  
}

class MMURequestManager extends Module {
  val io = IO(new Bundle {
    val inst_addr = Input(UInt(64.W))
    val inst_req_in = Input(Bool())
    val inst_req_out = Output(Bool())
    val inst_addr_ok_in = Input(Bool())
    val inst_addr_translated = Output(UInt(64.W))
    val data_addr = Input(UInt(64.W))
    val data_req_in = Input(Bool())
    val data_req_out = Output(Bool())
    val data_addr_ok_in = Input(Bool())
    val data_addr_translated = Output(UInt(64.W))
    val mmu_req = new MEM2MMU
  })
  val sIDLE :: sTRANSLATE :: sRESEND_REQ :: Nil = Enum(3)
  val state = RegInit(sIDLE)
  val addrReg = Reg(UInt(64.W))
  val PaddrReg = Reg(UInt(64.W))
  val isDataOp = Reg(Bool())
  io.inst_req_out := false.B
  io.data_req_out := false.B
  io.inst_addr_translated := 0.U
  io.data_addr_translated := 0.U
  io.mmu_req.reqReady := false.B
  io.mmu_req.reqVAddr := 0.U
  switch(state) {
    is(sIDLE) {
      when(io.data_req_in) {
        state := sTRANSLATE
        addrReg := io.data_addr
        isDataOp := true.B
      }.elsewhen(io.inst_req_in) {
        state := sTRANSLATE
        addrReg := io.inst_addr
        isDataOp := false.B
      }
    }
    is(sTRANSLATE) {
      io.mmu_req.reqVAddr := addrReg
      io.mmu_req.reqReady := true.B
      when(io.mmu_req.respValid) {
        state := sRESEND_REQ
        PaddrReg := io.mmu_req.respPAddr
      }
    }
    is(sRESEND_REQ) {
      when(isDataOp) {
        io.data_req_out := true.B
        io.data_addr_translated := PaddrReg
        when(io.data_addr_ok_in) {
          isDataOp := false.B
          state := sIDLE
        }
      }.otherwise{
        io.inst_req_out := true.B
        io.inst_addr_translated := PaddrReg
        when(io.inst_addr_ok_in) {
          state := sIDLE
        }
      }
    }
  }

}

object MMU extends App {
  val stage = new ChiselStage
  stage.emitVerilog(new MMU(false))
}
